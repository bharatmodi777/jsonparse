//
//  ViewController.m
//  ParseJson
//
//  Created by Admin on 15/03/16.
//  Copyright © 2016 Bharat Modi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSDictionary *dictTeamData;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"TestJson" ofType:@"json"];
    NSData *jsonData = [[NSData alloc] initWithContentsOfFile:filePath];
    
    NSError *error = nil;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    _dictTeamData = [[NSDictionary alloc] initWithDictionary:jsonDict];
    
    NSLog(@"%@", jsonDict);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_dictTeamData[@"Player_TeamList"] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //Team Name & Batting Team Name are outside Player_TeamList
    
    //Team Name
    UILabel *teamName = (UILabel*) [cell viewWithTag:10];
    teamName.text = _dictTeamData[@"A_TEAMNAME"];
    
    //Batting Team Name
    UILabel *battingTeamName = (UILabel*) [cell viewWithTag:20];
    battingTeamName.text = _dictTeamData[@"BATTING_TEAMNAME"];

    
    //Player Name & playerRole inside Player_TeamList
    //Player Name
    UILabel *playerName = (UILabel*) [cell viewWithTag:30];
    playerName.text = _dictTeamData[@"Player_TeamList"][indexPath.row][@"PLAYER_NAME"];

    //Player Role
    UILabel *playerRole = (UILabel*) [cell viewWithTag:40];
    playerRole.text = _dictTeamData[@"Player_TeamList"][indexPath.row][@"PLAYER_ROLE"];

    //Return cell
    return cell;
}

#pragma mark - UITableView Delegates
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return 150;
}


@end
