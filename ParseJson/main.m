//
//  main.m
//  ParseJson
//
//  Created by Admin on 15/03/16.
//  Copyright © 2016 Bharat Modi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
