//
//  AppDelegate.h
//  ParseJson
//
//  Created by Admin on 15/03/16.
//  Copyright © 2016 Bharat Modi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

